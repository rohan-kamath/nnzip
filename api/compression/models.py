from django.db import models

# Create your models here.

class User(models.Model):
	id = models.BigAutoField(db_column='ID', primary_key=True)
	firstname = models.CharField(db_column='FirstName', max_length=255)
	lastname = models.CharField(db_column='LastName', max_length=255)
	google_id = models.CharField(db_column='GoogleID', max_length=255)

	class Meta:
		managed = True
		db_table = 'User'

	def __str__(self):
		return self.firstname + " " + self.lastname
	
class File(models.Model):
	id = models.BigAutoField(db_column='ID', primary_key=True)
	uploadfile = models.FileField(upload_to='files/')
	user_id = models.CharField(db_column='UserID', max_length=255)
	compressed = models.BooleanField(db_column='Compressed', default=False)
from django.urls import path

from . import views

urlpatterns = [
    path("time", views.time, name="time"),
    path("files", views.files, name="files"),
    path("uploadJpeg", views.upload_jpeg, name="uploadJpeg"),
    path("uploadCompressed", views.upload_compressed, name="uploadCompressed"),
]
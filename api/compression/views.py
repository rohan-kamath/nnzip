from django.shortcuts import render
from django.http import HttpResponse, FileResponse
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
import json

from .models import File


# Create your views here.

def time(request):
    return HttpResponse(json.dumps(str(timezone.now())))

def files(request):
    files = File.objects.all()
    result = []
    google_id = request.GET.get('google_id', '')
    for f in files:
        if(f.google_id == google_id):
            result.append(f.uploadfile.name)
	
    return HttpResponse(json.dumps(result))

def upload_jpeg(request):
    if request.method == "POST":
        f = request.FILES["file"]
        user_id = request.GET.get('user_id', '')
        newfile = File(uploadfile = f, compressed = False, user_id = user_id)
        newfile.save()
        filename = newfile.uploadfile.path
        response = FileResponse(open(filename, 'rb'))
        return response
    else:
        return HttpResponse(json.dumps("GET upload_file"))

def upload_compressed(request):
    if request.method == "POST":
        f = request.FILES["file"]
        user_id = request.GET.get('user_id', '')
        newfile = File(uploadfile = f, compressed = True, user_id = user_id)
        newfile.save()
        return HttpResponse(json.dumps("Uploaded compressed file"))
    else:
        return HttpResponse(json.dumps("GET upload_file"))

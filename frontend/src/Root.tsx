import React, { FC, ReactNode } from "react";
import { Provider } from "react-redux";
import { HistoryRouter as Router } from "redux-first-history/rr6";
import { store, history } from "./Reducer";

import { setCurrentUser, setToken } from "./components/login/LoginActions";
import { isEmpty } from "./utils/Utils";

interface RootProps {
    children: ReactNode;
}

if (!isEmpty(localStorage.getItem("token"))) {
  store.dispatch(setToken(localStorage.getItem("token")!));
}
if (!isEmpty(localStorage.getItem("user"))) {
  const user = JSON.parse(localStorage.getItem("user")!);
  store.dispatch(setCurrentUser(user, ""));
}

const Root: FC<RootProps>  = ({ children }) => {
  return (
    <Provider store={store}>
      <Router history={history}>{children}</Router>
    </Provider>
  );
};

export default Root;
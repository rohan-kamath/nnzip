import {
    useLocation,
    useNavigate,
    useParams
} from "react-router-dom";

import axios from "axios";
import { toast } from "react-toastify";

export function withRouter(Component: any) {
    function ComponentWithRouterProp(props: any) {
        let location = useLocation();
        let navigate = useNavigate();
        let params = useParams();
        return (
        <Component
            {...props}
            router={{ location, navigate, params }}
        />
        );
    }
    return ComponentWithRouterProp;
}

export const setAxiosAuthToken = (token: any) => {
    if (typeof token !== "undefined" && token) {
      // Apply for every request
      axios.defaults.headers.common["Authorization"] = "Token " + token;
    } else {
      // Delete auth header
      delete axios.defaults.headers.common["Authorization"];
    }
};

export const toastOnError = (error: any) => {
    if (error.response) {
      // known error
      toast.error(JSON.stringify(error.response.data));
    } else if (error.message) {
      toast.error(JSON.stringify(error.message));
    } else {
      toast.error(JSON.stringify(error));
    }
};

export const isEmpty = (value: any) =>
    value === undefined ||
    value === null ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length === 0);

export function getCookie(name: string) : string {
  var cookieValue = "";
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i].trim();
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}
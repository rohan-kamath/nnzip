import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import axios, {AxiosRequestConfig} from 'axios';

function getCookie(name: string) : string{
  var cookieValue = "";
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i].trim();
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

function Secure() {
  const navigate = useNavigate();
  const [userDetails, setUserDetails] = useState<any>({});
  const [files, setFiles] = useState<any>([]);
  const [file, setFile] = useState<any>();

  const getUserDetails = async (accessToken: any) => {
    const response = await fetch(
      `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${accessToken}`
    );
    const data = await response.json();
    console.log(data);
    setUserDetails(data);
  };

  useEffect(() => {
    const accessToken = Cookies.get("access_token");

    if (!accessToken) {
      navigate("/");
    }

    getUserDetails(accessToken);
  }, [navigate]);

  useEffect(() => {
    axios.get(`http://localhost:8000/api/files?google_id=${userDetails.id}`).then((response: any) => {
      console.log(response.data);
      setFiles(response.data);
    })
  }, [userDetails]);

  function handleChange(event: any) {
    setFile(event.target.files[0])
  }

  function handleSubmit(event: any) {
    event.preventDefault()
    const url = `http://localhost:8000/api/uploadJpeg?google_id=${userDetails.id}`;
    const formData = new FormData();
    formData.append('file', file);
    formData.append('fileName', file.name);
    var csrftoken = getCookie('csrftoken');
    const config: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': csrftoken,
      },
      withCredentials: true,
      responseType: 'blob',
    };
    axios.post(url, formData, config).then((response: any) => {
      console.log(response.data);
      const href = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = href;
      link.setAttribute('download', file.name); //or any other extension
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    });
  }

  return (
    <>
      {userDetails ? (
        <div className="user-profile">
          <div className="card">
            <img
              src={userDetails.picture}
              alt={`${userDetails.given_name}'s profile`}
              className="profile-pic"
            />
            <p>Welcome</p>
            <h1 className="name">{userDetails.name}</h1>
            <p className="email">{userDetails.email}</p>
            <p className="locale">{`Locale: ${userDetails.locale}`}</p>
            <p className="files">files: {files}</p>
            <form onSubmit={handleSubmit} method="POST">
              <h1>Upload file to compress</h1>
              <input type="file" onChange={handleChange}/>
              <button type="submit">Upload</button>
            </form>
          </div>
        </div>
      ) : (
        <div>
          <h1>Loading...</h1>
        </div>
      )}
    </>
  );
}

export default Secure;
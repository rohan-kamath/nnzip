import React, { Component } from "react";
import { connect } from "react-redux";
import axios, {AxiosRequestConfig} from 'axios';

import { Container, Navbar, Nav } from "react-bootstrap";
import { logout } from "../login/LoginActions";
import { withRouter, getCookie } from "../../utils/Utils";

interface DashboardProps {
  logout: any;
  auth: any;
}

interface DashboardState {
  jpegFile: any;
  zipFile: any;
}

class Dashboard extends Component<DashboardProps, DashboardState> {
  constructor(props: DashboardProps) {
    super(props);

    this.state = {
      jpegFile: {},
      zipFile: {}
    };

    this.handleChangeJpeg = this.handleChangeJpeg.bind(this);
    this.handleChangeZip = this.handleChangeZip.bind(this);
    this.handleSubmitJpeg = this.handleSubmitJpeg.bind(this);
    this.handleSubmitZip = this.handleSubmitZip.bind(this);
  }

  handleChangeJpeg(event: any) {
    this.setState(state => ({jpegFile: event.target.files[0]}));
  }

  handleChangeZip(event: any) {
    this.setState(state => ({zipFile: event.target.files[0]}));
  }

  handleSubmitJpeg(event: any) {
    event.preventDefault()
    const url = `/api/uploadJpeg?user_id=${this.props.auth.id}`;
    const formData = new FormData();
    const jpegFile = this.state.jpegFile;
    formData.append('file', jpegFile);
    formData.append('fileName', jpegFile.name);
    var csrftoken = getCookie('csrftoken');
    const config: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': csrftoken,
      },
      withCredentials: true,
      responseType: 'blob',
    };
    axios.post(url, formData, config).then((response: any) => {
      console.log(response.data);
      const href = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = href;
      link.setAttribute('download', jpegFile.name); //or any other extension
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    });
  }

  handleSubmitZip(event: any) {
    event.preventDefault()
    const url = `/api/uploadCompressed?user_id=${this.props.auth.id}`;
    const formData = new FormData();
    const zipFile = this.state.zipFile;
    formData.append('file', zipFile);
    formData.append('fileName', zipFile.name);
    var csrftoken = getCookie('csrftoken');
    const config: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': csrftoken,
      },
      withCredentials: true,
      responseType: 'blob',
    };
    axios.post(url, formData, config).then((response: any) => {
      console.log(response.data);
      const href = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = href;
      link.setAttribute('download', zipFile.name); //or any other extension
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    });
  }

  onLogout = () => {
    this.props.logout();
  };

  render() {
    const { user } = this.props.auth;

    return (
      <div>
        <Navbar bg="light">
          <Navbar.Brand href="/">Home</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
              User: <b>{user.username}</b>
            </Navbar.Text>
            <Nav.Link onClick={this.onLogout}>Logout</Nav.Link>
          </Navbar.Collapse>
        </Navbar>
        <Container>
          <h1>Dashboard</h1>
          <div>
            <form onSubmit={this.handleSubmitJpeg} method="POST">
              <h1>Upload a JPEG file to compress it with neural networks</h1>
              <input type="file" onChange={this.handleChangeJpeg}/>
              <button type="submit">Upload</button>
            </form>
          </div>
          <div>
            <form onSubmit={this.handleSubmitZip} method="POST">
              <h1>Upload a file compressed here to uncompress it</h1>
              <input type="file" onChange={this.handleChangeZip}/>
              <button type="submit">Upload</button>
            </form>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  logout
})(withRouter(Dashboard));
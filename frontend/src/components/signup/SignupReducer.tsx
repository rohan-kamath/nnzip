import type { Reducer } from '@reduxjs/toolkit';
import {
    CREATE_USER_ERROR,
    CREATE_USER_SUBMITTED,
    CREATE_USER_SUCCESS
  } from "./SignupTypes";
  
interface SignupState {
    usernameError: string;
    passwordError: string;
    isSubmitted: boolean;
}

  // define the initial state of the signup store
const initialState: SignupState = {
    usernameError: "",
    passwordError: "",
    isSubmitted: false
};
  
  // define how action will change the state of the store
export const signupReducer: Reducer<SignupState> = (state: SignupState = initialState, action: any) => {
    switch (action.type) {
        case CREATE_USER_SUBMITTED:
        return {
            usernameError: "",
            passwordError: "",
            isSubmitted: true
        };
        case CREATE_USER_ERROR:
        const errorState = {
            usernameError: "",
            passwordError: "",
            isSubmitted: false
        };
        if (action.errorData.hasOwnProperty("username")) {
            errorState.usernameError = action.errorData["username"];
        }
        if (action.errorData.hasOwnProperty("password")) {
            errorState.passwordError = action.errorData["password"];
        }
        return errorState;
        case CREATE_USER_SUCCESS:
        return {
            usernameError: "",
            passwordError: "",
            isSubmitted: false
        };
        default:
        return state;
    }
}
import React, { Component, ChangeEvent } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import {
  Container,
  Button,
  Row,
  Col,
  Form,
  FormControl
} from "react-bootstrap";

import { signupNewUser } from "./SignupActions";
import { withRouter } from "../../utils/Utils";

interface SignupState {
    username: string;
    password: string;
}

interface SignupProps extends PropsFromRedux {}

class Signup extends Component<SignupProps, SignupState> {
  constructor(props: SignupProps) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  onChange = (e: ChangeEvent<HTMLInputElement>) => {
    if(e.target.name === "username"){
        this.setState({username: e.target.value});
    }
    if(e.target.name === "password"){
        this.setState({password: e.target.value});
    }
  };

  onSignupClick = () => {
    const userData = {
      username: this.state.username,
      password: this.state.password
    };
    //console.log("Sign up " + userData.username + " " + userData.password);
    this.props.signupNewUser(userData);
  };

  render() {
    return (
      <Container>
        <Row>
          <Col md="4">
            <h1>Sign up</h1>
            <Form>
              <Form.Group controlId="usernameId">
                <Form.Label>User name</Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  placeholder="Enter user name"
                  value={this.state.username}
                  onChange={this.onChange}
                />
                <FormControl.Feedback type="invalid"></FormControl.Feedback>
              </Form.Group>

              <Form.Group controlId="passwordId">
                <Form.Label>Your password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  placeholder="Enter password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                <Form.Control.Feedback type="invalid"></Form.Control.Feedback>
              </Form.Group>
            </Form>
            <Button 
              color="primary"
              onClick={this.onSignupClick}  
            >Sign up</Button>
            <p className="mt-2">
              Already have account? <Link to="/login">Login</Link>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  createUser: state.createUser
});

const connector = connect(mapStateToProps, {
  signupNewUser
});

type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(withRouter(Signup));
import React, { Component, ReactNode } from "react";
import Root from "./Root";
import { Route, Routes } from "react-router-dom";
import Landing from "./components/Landing";
import Secure from "./components/Secure";
import Signup from "./components/signup/Signup";
import Login from "./components/login/Login";
import Dashboard from "./components/dashboard/Dashboard";
import { ToastContainer } from "react-toastify";
import axios from "axios";

import PrivateRoute from "./utils/PrivateRoute";

axios.defaults.baseURL = "http://127.0.0.1:8000";

class App extends Component {
  render(): ReactNode {
    return (
      <div>
        <Root>
          <Routes>
            <Route path="/signup" element={<Signup />} />
            <Route path="/login" element={<Login />} />
            <Route 
              path="/dashboard" 
              element={
                <PrivateRoute>
                  <Dashboard />
                </PrivateRoute>
              } 
            />
            <Route path="/" element={<Landing />} />
            <Route path="/secure" element={<Secure />} />
          </Routes>
        </Root>
        <ToastContainer hideProgressBar={true} newestOnTop={true} />
      </div>
    );
  }
}

export default App;
//created referencing https://codesandbox.io/s/redux-first-history-demo-rr6-forked-wvst19
//

import { createReduxHistoryContext } from "redux-first-history";
import createSagaMiddleware from "redux-saga";
import { Middleware } from "redux";
import { createBrowserHistory } from "history";
import { signupReducer } from "./components/signup/SignupReducer";
import { loginReducer } from "./components/login/LoginReducer"; 

import { configureStore } from '@reduxjs/toolkit'

const sagaMiddleware = createSagaMiddleware();

const {
    createReduxHistory,
    routerMiddleware,
    routerReducer
  } = createReduxHistoryContext({ history: createBrowserHistory() });

const middleware: Middleware[] = [sagaMiddleware, routerMiddleware];

const reducer = {
    router: routerReducer,
    createUser: signupReducer,
    auth: loginReducer,
}

export const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middleware)
})

export const history = createReduxHistory(store);
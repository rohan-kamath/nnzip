## Installation for dev

1. Copy `.env.dev.exemple` to `.env.dev` and edit values.

2. Run `docker compose --env-file .env.dev up --build`

3. Visit `http://localhost:8000` or `http://localhost:8000/admin` or `http://localhost:3000`